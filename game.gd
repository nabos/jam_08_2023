extends Control

# Buttons
@onready var research_button = $menu/button_research
@onready var geopolitics_button = $menu/button_geopolitics

# Windows
@onready var research_window = $research
@onready var geopolitics_window = $geopolitics
@onready var cycle_result_window = $cycle_result

class ResearchNode:
	var id: int
	var name: String
	var level: int
	var children: Array
	var discovered: bool

var magic_graph: ResearchNode = null
var current_subject: ResearchNode = null
	
func list_available_nodes():
	pass

func _ready():
	# Level 6
	var backrooms = ResearchNode.new()
	backrooms.id = 46
	backrooms.name = "Backrooms"
	backrooms.level = 6
	backrooms.discovered = false
	
	var galaxy_move = ResearchNode.new()
	galaxy_move.id = 44
	galaxy_move.name = "Déplacement galactique"
	galaxy_move.level = 6
	galaxy_move.discovered = false
	
	var temporal_communication = ResearchNode.new()
	temporal_communication.id = 47
	temporal_communication.name = "Communication temporelle"
	temporal_communication.level = 6
	temporal_communication.discovered = false
	
	var god_mode = ResearchNode.new()
	god_mode.id = 48
	god_mode.name = "Mode créatif"
	god_mode.level = 6
	god_mode.discovered = false
	
	var hyperspatial_galactic_trebuchet = ResearchNode.new()
	hyperspatial_galactic_trebuchet.id = 45
	hyperspatial_galactic_trebuchet.name = "Trébuchet gravitationel hyperspatial"
	hyperspatial_galactic_trebuchet.level = 6
	hyperspatial_galactic_trebuchet.discovered = false
	
	# Level 5
	var galactic_highway = ResearchNode.new()
	galactic_highway.id = 43
	galactic_highway.name = "Autoroutes galactiques"
	galactic_highway.level = 5
	galactic_highway.children = [galaxy_move]
	galactic_highway.discovered = false
	
	# Level 4
	var pocket_dimension = ResearchNode.new()
	pocket_dimension.id = 41
	pocket_dimension.name = "Dimension de poche"
	pocket_dimension.level = 4
	pocket_dimension.children = [backrooms]
	pocket_dimension.discovered = false
	
	var mind_museum = ResearchNode.new()
	mind_museum.id = 42
	mind_museum.name = "Musée mental"
	mind_museum.level = 4
	mind_museum.children = [god_mode]
	mind_museum.discovered = false
	
	var future_trip = ResearchNode.new()
	future_trip.id = 40
	future_trip.name = "Voyage dans le futur"
	future_trip.level = 4
	future_trip.children = [temporal_communication]
	future_trip.discovered = false
	
	# Level 3
	var instant_reflect = ResearchNode.new()
	instant_reflect.id = 16
	instant_reflect.name = "Reflection instantanée"
	instant_reflect.level = 3
	instant_reflect.discovered = false
	
	var contracted_transport = ResearchNode.new()
	contracted_transport.id = 36
	contracted_transport.name = "Transport contracté"
	contracted_transport.level = 3
	contracted_transport.children = [galactic_highway]
	contracted_transport.discovered = false
	
	var hyperspace_movement = ResearchNode.new()
	hyperspace_movement.id = 35
	hyperspace_movement.name = "Déplacement hyperspatial"
	hyperspace_movement.level = 3
	hyperspace_movement.children = [pocket_dimension]
	hyperspace_movement.discovered = false
	
	var mind_palace = ResearchNode.new()
	mind_palace.id = 34
	mind_palace.name = "Palais mental"
	mind_palace.level = 3
	mind_palace.children = [mind_museum]
	mind_palace.discovered = false
	
	var telepathic_communication = ResearchNode.new()
	telepathic_communication.id = 33
	telepathic_communication.name = "Communication télépathique"
	telepathic_communication.level = 3
	telepathic_communication.children = [temporal_communication]
	telepathic_communication.discovered = false
	
	var autosteering = ResearchNode.new()
	autosteering.id = 31
	autosteering.name = "Autoguidage"
	autosteering.level = 3
	autosteering.discovered = false
	
	var complex_assembly = ResearchNode.new()
	complex_assembly.id = 32
	complex_assembly.name = "Assemblage complexe"
	complex_assembly.level = 3
	complex_assembly.discovered = false
	
	var artificial_sun = ResearchNode.new()
	artificial_sun.id = 30
	artificial_sun.name = "Soleil artificiel"
	artificial_sun.level = 3
	artificial_sun.children = [hyperspatial_galactic_trebuchet]
	artificial_sun.discovered = false
	
	var grandfather_paradox = ResearchNode.new()
	grandfather_paradox.id = 29
	grandfather_paradox.name = "Paradoxe du Grand-père"
	grandfather_paradox.level = 3
	grandfather_paradox.children = [temporal_communication]
	grandfather_paradox.discovered = false
	
	var slow_metabolism = ResearchNode.new()
	slow_metabolism.id = 27
	slow_metabolism.name = "Métabolisme ralentis"
	slow_metabolism.level = 3
	slow_metabolism.children = [future_trip, galaxy_move]
	slow_metabolism.discovered = false
	
	var fast_metabolism = ResearchNode.new()
	fast_metabolism.id = 26
	fast_metabolism.name = "Métabolisme accéléré"
	fast_metabolism.level = 3
	fast_metabolism.discovered = false
	
	var fast_machinery = ResearchNode.new()
	fast_machinery.id = 25
	fast_machinery.name = "Machinerie accélérée"
	fast_machinery.level = 3
	fast_machinery.discovered = false
	
	var perfect_recycling = ResearchNode.new()
	perfect_recycling.id = 39
	perfect_recycling.name = "Recyclage parfait"
	perfect_recycling.level = 3
	perfect_recycling.children = [god_mode]
	perfect_recycling.discovered = false
	
	var precision_modeling = ResearchNode.new()
	precision_modeling.id = 38
	precision_modeling.name = "Faconnage de précision"
	precision_modeling.level = 3
	precision_modeling.children = [god_mode]
	precision_modeling.discovered = false
	
	var terraformation = ResearchNode.new()
	terraformation.id = 37
	terraformation.name = "Terraformation"
	terraformation.level = 3
	terraformation.children = [god_mode, backrooms]
	terraformation.discovered = false
	
	var time_prison = ResearchNode.new()
	time_prison.id = 28
	time_prison.name = "Prison temporelle"
	time_prison.level = 3
	time_prison.children = [backrooms]
	time_prison.discovered = false
	
	# Level 2 - Space
	var space_dilation = ResearchNode.new()
	space_dilation.id = 21
	space_dilation.name = "Dilatation spatiale"
	space_dilation.level = 2
	space_dilation.children = [backrooms]
	space_dilation.discovered = false
	
	var space_contraction = ResearchNode.new()
	space_contraction.id = 20
	space_contraction.name = "Contraction spatiale"
	space_contraction.level = 2
	space_contraction.children = [contracted_transport, backrooms]
	space_contraction.discovered = false
	
	var hyperspace = ResearchNode.new()
	hyperspace.id = 19
	hyperspace.name = "Hyperespace"
	hyperspace.level = 2
	hyperspace.children = [hyperspace_movement, mind_palace]
	hyperspace.discovered = false
	
	# Level 2 - Mind
	var mind_projection = ResearchNode.new()
	mind_projection.id = 18
	mind_projection.name = "Projection mentale"
	mind_projection.level = 2
	mind_projection.children = [mind_museum]
	mind_projection.discovered = false
	
	var perfect_memory = ResearchNode.new()
	perfect_memory.id = 17
	perfect_memory.name = "Mémorisation parfaite"
	perfect_memory.level = 2
	perfect_memory.children = [mind_palace]
	perfect_memory.discovered = false
	
	var will_anihilation = ResearchNode.new()
	will_anihilation.id = 15
	will_anihilation.name = "Anihilation de volonté"
	will_anihilation.level = 2
	will_anihilation.discovered = false
	
	var telepathy = ResearchNode.new()
	telepathy.id = 14
	telepathy.name = "Télépathie"
	telepathy.level = 2
	telepathy.children = [telepathic_communication]
	telepathy.discovered = false
	
	var telekinesy = ResearchNode.new()
	telekinesy.id = 13
	telekinesy.name = "Télékinesie"
	telekinesy.level = 2
	telekinesy.children = [grandfather_paradox]
	telekinesy.discovered = false
	
	#Level 2 - Gravity
	var heavy_transport = ResearchNode.new()
	heavy_transport.id = 9
	heavy_transport.name = "Transport lourd"
	heavy_transport.level = 2
	heavy_transport.discovered = false
	
	var gravitationnal_deflect = ResearchNode.new()
	gravitationnal_deflect.id = 10
	gravitationnal_deflect.name = "Déflection gravitationnelle"
	gravitationnal_deflect.level = 2
	gravitationnal_deflect.discovered = false
	
	var gravitationnal_persistency = ResearchNode.new()
	gravitationnal_persistency.id = 11
	gravitationnal_persistency.name = "Persistance gravitationelle"
	gravitationnal_persistency.level = 2
	gravitationnal_persistency.children = [artificial_sun, autosteering, complex_assembly, galaxy_move]
	gravitationnal_persistency.discovered = false
	
	var gravity_launch = ResearchNode.new()
	gravity_launch.id = 12
	gravity_launch.name = "Lancé gravitationnel"
	gravity_launch.level = 2
	gravity_launch.children = [hyperspatial_galactic_trebuchet]
	gravity_launch.discovered = false
	
	# Level 2 - Time
	var past_observation = ResearchNode.new()
	past_observation.id = 8
	past_observation.name = "Observation du passé"
	past_observation.level = 2
	past_observation.children = [grandfather_paradox]
	past_observation.discovered = false
	
	var slowdown_bubble = ResearchNode.new()
	slowdown_bubble.id = 7
	slowdown_bubble.name = "Bulle de ralentissement"
	slowdown_bubble.level = 2
	slowdown_bubble.children = [slow_metabolism, time_prison]
	slowdown_bubble.discovered = false
	
	var speedup_bubble = ResearchNode.new()
	speedup_bubble.id = 6
	speedup_bubble.name = "Bulle d'accélération"
	speedup_bubble.level = 2
	speedup_bubble.children = [fast_machinery, fast_metabolism, instant_reflect]
	speedup_bubble.discovered = false
	
	# Level 2 - Matter
	var densification = ResearchNode.new()
	densification.id = 24
	densification.name = "Densification"
	densification.level = 2
	densification.children = [fast_machinery, artificial_sun]
	densification.discovered = false
	
	var transmutation = ResearchNode.new()
	transmutation.id = 23
	transmutation.name = "Transmutation"
	transmutation.level = 2
	transmutation.children = [perfect_recycling]
	transmutation.discovered = false
	
	var modeling = ResearchNode.new()
	modeling.id = 22
	modeling.name = "Faconnage"
	modeling.level = 2
	modeling.children = [precision_modeling, terraformation]
	modeling.discovered = false
	
	# Level 1
	var time = ResearchNode.new()
	time.id = 1
	time.name = "Temps"
	time.level = 1
	time.children = [past_observation, slowdown_bubble, speedup_bubble]
	time.discovered = false
	
	var matter = ResearchNode.new()
	matter.id = 5
	matter.name = "Matière"
	matter.level = 1
	matter.children = [densification, transmutation, modeling, mind_projection]
	matter.discovered = false
	
	var gravity = ResearchNode.new()
	gravity.id = 2
	gravity.name = "Gravité"
	gravity.level = 1
	gravity.children = [heavy_transport, gravitationnal_deflect, gravitationnal_persistency, gravity_launch, telekinesy]
	gravity.discovered = false
	
	var mind = ResearchNode.new()
	mind.id = 3
	mind.name = "Esprit"
	mind.level = 1
	mind.children = [mind_projection, perfect_memory, instant_reflect, will_anihilation, telepathy, telekinesy]
	mind.discovered = false
	
	var space = ResearchNode.new()
	space.id = 4
	space.name = "Espace"
	space.level = 1
	space.children = [space_dilation, space_contraction, hyperspace]
	space.discovered = false
	
	# Level 0
	var magic_graph = ResearchNode.new()
	magic_graph.name = "Magie"
	magic_graph.level = 0
	magic_graph.children = [space, mind, gravity, time, matter]
	magic_graph.discovered = true

func _process(delta):
	pass

func _on_research_toggled(button_pressed):
	if button_pressed:
		geopolitics_button.button_pressed = false
	research_window.visible = button_pressed

func _on_geopolitics_toggled(button_pressed):
	if button_pressed:
		research_button.button_pressed = false
	geopolitics_window.visible = button_pressed

func close_windows():
	geopolitics_button.button_pressed = false
	research_button.button_pressed = false
	cycle_result_window.visible = false
	
func update_research_subject():
	#updating research subject display
	pass

func select_new_research_subject():
	# get a random new subject from the research tree
	current_subject = null

func start_cycle():
	close_windows()
	update_research_subject()

func end_cycle():
	close_windows()
